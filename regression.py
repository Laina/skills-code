import matplotlib.pyplot as plt

x = [454, 449, 330, 538, 525, 636, 516, 621, 420, 474]
y = [1316, 1259, 1007, 1634, 1535, 1763, 1363, 1814, 1212, 1340]


def findA(x, y):
    mean = lambda ls: sum(ls) / len(ls)
    xbar = mean(x)
    ybar = mean(y)
    n = len(x)

    sxx = sum(
        [i**2 for i in x]
        ) - sum(
                x 
                )**2/n

    sxy = sum(
        [
            x[i] * y[i] for i in range(len(x))
            ]
        ) - (
                (sum(x) * sum(y))
                )/n

    b = sxy / sxx

    a = ybar - b*xbar
    return a, b


(a, b) = findA(x, y)
yHat = [i*b + a for i in x]

print(f"{a}\t{b}")
plt.scatter(x, y)
plt.plot(x, 
         yHat)

for i in range(len(y)):
    plt.plot([x, x],
             [yHat[i], y[i]])

plt.show()
