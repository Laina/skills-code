
def find_height(x):
    return  ((x ** 4)
            - (2 * x)
            + 1)


def trapezium_area(a: int, b: int, h: int):
    return ((a + b) * h
            /2)

curr_area = 0
for _ in range(0, 20):

    i = 0

    a = find_height(i)
    b = find_height(i + 0.2)

    curr_area += trapezium_area(a, b, 0.2)
    i += 0.2


print(curr_area)
