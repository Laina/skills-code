import math
from time import time
def sieve_alg(deg):
    pre = time()
    allnums = [True] * (deg + 1)
    allnums[0] = allnums[1] = False
    
    for i in range(2, int(math.sqrt(deg)) + 1):
        if allnums[i]:
            for j in range(i * i, deg + 1, i):
                allnums[j] = False
                
    primes = [i for i in range(2, deg + 1) if allnums[i]]
    after = time()
    print(after - pre)

sieve_alg(100000000)

