import matplotlib.pyplot as plt

f_y = []
f_x = []

h_x = []
h_y = []

fh_x = []
fh_y = []

def f(x):

    return 0.2 * (x ** 5
                  - x ** 2
                  + x)

def actual_deriv(x):
    return (x ** 4
            - (2 * x)
            + 1)


def find_deriv1(x, h):
    try:
        return (1 / h) * (f(x + h)
                    - f(x)) 
    except:
        raise Exception()


def find_deriv2(x, h):
    try:
        return (1 / h) * (f(x)
                      - f(x - h)) 
    except:
        raise Exception()


def find_deriv3(x, h):
    try:
        return (1 / h) * (
                f(x + (h / 2))
                - f(x - (h / 2))
                ) 
    except:
        raise Exception()

x = 0.1

for i in range(100):
    f_y.append(
            find_deriv3(f(x), i)
            )
    f_x.append(i)


for i in range(100):
    h_y.append(
            actual_deriv(i)
            )
    h_x.append(i)


for i in range(100):
    fh_x.append(
            (f_y[i] ** 2) - (h_y[i] ** 2)
            )
    fh_y.append(i)





plt.plot(f_x, f_y)
plt.show()

plt.plot(h_x, h_y)
plt.show()

plt.plot(fh_x, fh_y)
plt.show()


