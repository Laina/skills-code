import math,turtle
turtle.reset()
turtle.tracer(30)

def koch(n, length):
    if n>0:
        koch(n-1, length)
        turtle.left(60)
        koch(n-1, length)
        turtle.right(120)
        turtle.forward(length)
        koch(n-1, length)
        turtle.left(60)
        turtle.forward(length)
        koch(n-1, length)
    
turtle.penup()
turtle.setpos(-250, 250)
turtle.pendown()
step = 5
colour = 0
while True:
    length = 270*3
    for _ in range(step):
        length /= 3
    for _ in range(3):
        turtle.forward(length)
        koch(step, length)
        turtle.right(math.pi*50)
        colour += 0.01

        turtle.pencolor(colour - colour//1, (colour/2) - colour//1, (colour*2) - colour//1)
