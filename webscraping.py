import requests
import bs4
def getExponents(links):
        if type(links) == type(None): return False
        return "/report_exponent/" in links

r = requests.get("https://mersenne.org/primes", auth = ("user", "pass"))
html = bs4.BeautifulSoup(r.text, "html.parser")
linklist = []
for link in html.find_all('a'):
    linklist.append(link.get('href'))

linklist = list(filter(getExponents, linklist))
newlinklist = []
for i in linklist:
    i = i.strip("/report_exponent/").strip("?exp_lo=")
    newlinklist.append(i) 
newlinklist = newlinklist[1:]
print(newlinklist)
